//
//  hangmanTests.swift
//  hangmanTests
//
//  Created by Deepti Babajee on 20/11/2018.
//  Copyright © 2018 Deepti Babajee. All rights reserved.
//

import XCTest

@testable import hangman

class hangmanTests: XCTestCase {
    
    private var sutRound:GameEngine?

    override func setUp() {
        let testWordList = [["CYCLONE","cyclic winds"],["WORDTWO","hint for word2"],["WORDTHREE","hint for word3"]]
        
        sutRound = GameEngine(wordsAndHints: testWordList)
    }
    override func tearDown() {
        sutRound = nil
    }
    
    func testHiddenLettersDidInintialise(){
        //Arrange
        let expectedResult = "-------"
        //Act
        let actualResult = sutRound!.getHiddenLetters()
        //Assert
        XCTAssertEqual(expectedResult,actualResult, "CYCLONE secret word should generate -------")
    }
    
    func testVerification_wrongLetterReturnsFalse1of2() {
        //Arrange
        let inputLetter:Character = "A"
        let expectedResult = false
        //Act
        let actualResult = sutRound!.checkLetter(inputLetter)
        //Assert
        XCTAssertEqual(expectedResult, actualResult, "A is not present in Cyclone according to checkLetter()")
    }
    
    func testVerification_wrongLetterReturnsFalse2of2(){
        //Arrange
        let inputLetter:Character = "B"
        let expectedResult = false
        //Act
        let actualResult = sutRound!.checkLetter(inputLetter)
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "B is not present in Cyclone according to checkLetter()")
        
    }
    
    func testVerification_correctLetterReturnsTrue1of2(){
        //Arrange
        let inputLetter:Character = "C"
        let expectedResult = true
        //Act
        let actualResult = sutRound!.checkLetter(inputLetter)
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "C is present in Cyclone according to checkLetter()")
        
    }
    
    func testVerification_correctLetterReturnsTrue2of2(){
        //Arrange
        let inputLetter:Character = "N"
        let expectedResult = true
        //Act
        let actualResult = sutRound!.checkLetter(inputLetter)
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "N is present in Cyclone according to checkLetter()")
        
    }
    
    func testVerification_theIncorrectLetterIsRecorded1of2(){
        //Arrange
        let inputLetter:Character = "A"
        let expectedResult = "A"
        //Act
        let _ = sutRound!.checkLetter(inputLetter)
        let actualResult = sutRound!.getIncorrectLettersPlayed()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "A is a misplay therefore has to be in IncorrectLettersPlayed")
        
    }
    
    func testVerification_theIncorrectLetterIsRecorded2of2(){
        //Arrange
        let inputLetter1:Character = "A"
        let inputLetter2:Character = "B"
        let inputLetter3:Character = "D"
        let expectedResult = "ABD"
        //Act
        let _ = sutRound!.checkLetter(inputLetter1)
        let _ = sutRound!.checkLetter(inputLetter2)
        let _ = sutRound!.checkLetter(inputLetter3)
        let actualResult = sutRound!.getIncorrectLettersPlayed()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "A,B,D are misplays therefore have to be in IncorrectLettersPlayed")
        
    }
    
    func testVerification_IncorrectLettersIncrementChancesProperly(){
        //Arrange
        let inputLetter1:Character = "A"
        let inputLetter2:Character = "B"
        let expectedResult = 8
        //Act
        let _ = sutRound!.checkLetter(inputLetter1)
        let _ = sutRound!.checkLetter(inputLetter2)
        let actualResult = sutRound!.getChances()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "A,B are misplays therefore decrement chances to 8")
        
    }
    
    func testVerification_correctLetter_AcceptedLettersUpdatedProperly1of2(){
        //Arrange
        let inputLetter:Character = "N"
        let expectedResult = "N"
        //Act
        let _ = sutRound!.checkLetter(inputLetter)
        let actualResult = sutRound!.getAcceptedLetters()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "N is present in Cyclone according to checkLetter() and is in AcceptedLetters")
        
    }
    
    func testVerification_correctLetter_AcceptedLettersUpdatedProperly2of2(){
        //Arrange
        let inputLetter:Character = "E"
        let expectedResult = "E"
        //Act
        let _ = sutRound!.checkLetter(inputLetter)
        let actualResult = sutRound!.getAcceptedLetters()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "E is present in Cyclone according to checkLetter() and is in AcceptedLetters")
        
    }
    
    func testVerification_correctLetterHiddenLettersUpdatedProperly(){
        //Arrange
        let inputLetter:Character = "C"
        let expectedResult = "C-C----"
        //Act
        let _ = sutRound!.checkLetter(inputLetter)
        let actualResult = sutRound!.getHiddenLetters()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "C is correct and updates hiddenletters to C-C----")
    }
    
    func testWin(){
        //Arrange
        let expectedResult = true
        //Act
        let _ = sutRound!.checkLetter("C")
        let _ = sutRound!.checkLetter("Y")
        let _ = sutRound!.checkLetter("L")
        let _ = sutRound!.checkLetter("O")
        let _ = sutRound!.checkLetter("N")
        let _ = sutRound!.checkLetter("E")
        let actualResult = sutRound!.checkWin()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "C,Y,L,O,N,E has been played thus checkWin must return true")
    }
    
    func testLose(){
        //Arrange
        let expectedResult = true
        //Act
        let _ = sutRound!.checkLetter("Q")
        let _ = sutRound!.checkLetter("W")
        let _ = sutRound!.checkLetter("R")
        let _ = sutRound!.checkLetter("T")
        let _ = sutRound!.checkLetter("U")
        let _ = sutRound!.checkLetter("S")
        let _ = sutRound!.checkLetter("D")
        let _ = sutRound!.checkLetter("F")
        let _ = sutRound!.checkLetter("G")
        let _ = sutRound!.checkLetter("H")
        let actualResult = sutRound!.checkLose()
        //Assert
        XCTAssertEqual(expectedResult, actualResult,  "10 incorrect Letters have been played thus checkLose must return true")
    }
    
}
