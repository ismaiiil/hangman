//
//  hangmanUITests.swift
//  hangmanUITests
//
//  Created by Deepti Babajee on 20/11/2018.
//  Copyright © 2018 Deepti Babajee. All rights reserved.
//

import XCTest

class hangmanUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testInputCharacter_hiddenlettersisupdated() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        app.otherElements.containing(.staticText, identifier:"Chances").children(matching: .textField).element.tap()
        let cKey = app.keyboards.keys["c"]
        cKey.tap()
        app.buttons["Go!"].tap()
        
        XCTAssertTrue(XCUIApplication().staticTexts["C-C----"].exists,"inputing C as text in the inout text creates C-C----")
        

        
    }
	
}
