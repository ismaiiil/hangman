//
//  gameEngine.swift
//  hangman
//
//  Created by Deepti Babajee on 20/11/2018.
//  Copyright © 2018 Deepti Babajee. All rights reserved.
//

import Foundation

class GameEngine{
    private var _incorrectLettersPlayed:[Character] = []
    private var _chances = 10
    private var _hiddenLetters:[Character] = []
    private var _secretWord:[Character] = []
    private var _secretWordBuffer:[Character] = []
    private var _acceptedLetters:[Character] = []
    private var _hint:String = ""
    private var _numberOfWords = 0
    private var _currentIndexOfWord = 0
    private var _wordsAndHints:[[String]] = []
    
    
    init(wordsAndHints:[[String]]) {
        self._wordsAndHints = wordsAndHints
        self._secretWord = Array(wordsAndHints[0][0])
        self._secretWordBuffer = Array(wordsAndHints[0][0])
        self._hint = wordsAndHints[0][1]
        self._numberOfWords = wordsAndHints.count
        init_hiddenLetters()
        
    }
    
    func getHiddenLetters() -> String{
        return String(_hiddenLetters)
    }
    
    func getIncorrectLettersPlayed()->String{
        return String(_incorrectLettersPlayed)
    }
    
    func getChances()->Int{
        return _chances
    }
    
    func getAcceptedLetters()->String{
        return String(_acceptedLetters)
    }
    
    func getHint()->String{
        return String(_hint)
    }
    
    func init_hiddenLetters(){
        for char in _secretWord{
            if (char != " "){
                _hiddenLetters.append("-")
            }else{
                _hiddenLetters.append(" ")
            }
        }
    }
    
    func checkLetter(_ inputLetter:Character) -> Bool {
        var atLeastOneMatched:Bool = false
        for (index,char) in _secretWordBuffer.enumerated(){
            if (char == inputLetter){
                //remove and replace the letter from secretWord Buffer
                _secretWordBuffer[index] = "-"
                
                //put the letter into the hidden letters at the necesary index
                _hiddenLetters[index] = char
                
                //append to accepted letters
                _acceptedLetters.append(char)
                
                atLeastOneMatched = true
                
                if checkWin() {
                    if(_currentIndexOfWord + 1 < _numberOfWords){
                        _currentIndexOfWord += 1
                        self._secretWord = Array(_wordsAndHints[_currentIndexOfWord][0])
                        self._secretWordBuffer = Array(_wordsAndHints[_currentIndexOfWord][0])
                        self._hint = _wordsAndHints[_currentIndexOfWord][1]
                        _incorrectLettersPlayed = []
                        _acceptedLetters = []
                        _hiddenLetters = []
                        init_hiddenLetters()
                    }
                }
            }
        }
        if (!atLeastOneMatched) {
            //add an incorrect counter(player loses a chance)
            _chances -= 1
            _incorrectLettersPlayed.append(inputLetter)
        }
        return atLeastOneMatched
    }
    
    func checkWin()->Bool{
        let hasWon = String(_secretWord) == String(_hiddenLetters)
        return hasWon
    }
    
    func checkBeatGame()-> Bool{
        return _numberOfWords == _currentIndexOfWord
    }
    
    func checkLose()->Bool{
        return _chances == 0
    }
    
    
}
