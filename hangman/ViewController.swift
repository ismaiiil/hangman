//
//  ViewController.swift
//  hangman
//
//  Created by Deepti Babajee on 20/11/2018.
//  Copyright © 2018 Deepti Babajee. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITextFieldDelegate {
    
    var testRound:GameEngine?
    var inputLetter:String?

    @IBAction func btnGo(_ sender: Any) {
        inputLetter = txtInput.text?.uppercased()
        if inputLetter!.count > 0{
            let _ = testRound?.checkLetter(Array(inputLetter!)[0])
            updateUI()
        }
        
        
    }
    
    
    
    @IBAction func btnHint(_ sender: Any) {
        txtBtnHint.setTitle(testRound?.getHint(), for: .normal)
    }
    
    @IBOutlet weak var txtBtnHint: UIButton!
    
    @IBOutlet weak var lblChancesLeft: UILabel!
    
    
    @IBOutlet weak var imgStatus: UIImageView!
    
    
    
    @IBOutlet weak var txtInput: UITextField!
    
   
    
    @IBAction func txtInputChanged(_ sender: Any) {
    }
    
    
    @IBOutlet weak var lblMisses: UILabel!
    
    
    @IBOutlet weak var lblSecretWord: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        txtInput.delegate = self
        txtInput.keyboardType = .alphabet
        testRound = GameEngine(wordsAndHints: getWordsFromPlist())
        updateUI()
    }
    
    //UI Delegates:
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet(charactersIn: "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm")
        if textField == txtInput {
            let inputCharacterSet = CharacterSet(charactersIn: string)
            guard let text = textField.text else { return true }
            let count = text.count + string.count - range.length
            return count <= 1 && allowedCharacters.isSuperset(of: inputCharacterSet)
        }
        return false
    }
    
    func updateUI(){
        lblSecretWord.text = testRound!.getHiddenLetters()
        lblChancesLeft.text = String(testRound!.getChances())
        lblMisses.text = testRound!.getIncorrectLettersPlayed()
        txtInput.text = ""
    }
    
    func getWordsFromPlist()->[[String]]{
        let url = Bundle.main.url(forResource: "Words", withExtension: "plist")!
        let soundsData = try! Data(contentsOf: url)
        let wordsAndHints = try! PropertyListSerialization.propertyList(from: soundsData, options: [], format: nil) as? [[String]]
        return wordsAndHints!
    }


}

